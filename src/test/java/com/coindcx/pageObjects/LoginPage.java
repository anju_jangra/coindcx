package com.coindcx.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {
    WebDriver driver;

    public LoginPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "/html/body/app-root/div/div[1]/cdcx-new-home-page/cdcx-new-home-header/div[1]/div/div[2]/p/a")
    @CacheLookup
    WebElement btnForLoginPage;

    @FindBy(id = "mat-input-0")
    @CacheLookup
    WebElement txtUserName;

    @FindBy(id = "mat-input-1")
    @CacheLookup
    public WebElement txtPassword;

    @FindBy(xpath = "/html/body/app-root/div/div[1]/cdcx-signin-signup/div/div/div[2]/main/section/div[1]/form/div[5]")
    @CacheLookup
     public WebElement loginBtn;

    @FindBy(xpath = "/html/body/app-root/div/div[1]/cdcx-signin-signup/div/div/div[2]/main/section/div[1]/form/div[5]/button")
    @CacheLookup
    public WebElement disabledloginbtn;

    //for invalid mailid
    @FindBy(id = "mat-error-0")
    @CacheLookup
    public WebElement invalidEmail;

    //for invalid credentials
    @FindBy(xpath = "/html/body/app-root/div/div[1]/cdcx-signin-signup/div/div/div[2]/main/section/div[1]/form/div[5]/p")
    @CacheLookup
    public WebElement invalidCredentials;

    public void clickBtnForLoginPage(){ btnForLoginPage.click(); }

    public void setUserName(String userName){
        txtUserName.sendKeys(userName);
    }

    public void setPassword(String pwd){
        txtPassword.sendKeys(pwd);
    }

    public void clickLoginBtn(){ loginBtn.click(); }

}
