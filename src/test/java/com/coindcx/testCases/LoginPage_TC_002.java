package com.coindcx.testCases;

import com.coindcx.pageObjects.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPage_TC_002 extends BaseClass{
    //Validate login with invalid mailid
    @Test
    public void testLogin() throws InterruptedException {
        LoginPage lp = new LoginPage(driver);
        Thread.sleep(6000);
        lp.clickBtnForLoginPage();
        Thread.sleep(6000);
        lp.setUserName("userName");
        lp.txtPassword.click();
        Assert.assertTrue(lp.invalidEmail.isDisplayed(),"Error message is not displayed");
    }
}
