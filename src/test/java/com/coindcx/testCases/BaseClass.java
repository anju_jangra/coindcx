package com.coindcx.testCases;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseClass {
    public String baseURL="https://coindcx.com";
    public String userName="anjujangra2128@gmail.com";
    public String password="anjujangra2128";
    public static WebDriver driver;

    @BeforeClass
    public void launchDriver(){
        WebDriverManager.chromedriver().setup();
        driver= new ChromeDriver();
        driver.get(baseURL);
        System.out.println("object : "+driver);
    }

    @AfterClass
    public void closeDriver(){
        driver.close();
    }

}
