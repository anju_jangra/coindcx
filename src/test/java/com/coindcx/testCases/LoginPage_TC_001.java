package com.coindcx.testCases;

import com.coindcx.pageObjects.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class LoginPage_TC_001 extends BaseClass {
    //For valid credentials it is not possible for me to automate
    //Validate login with invalid credentials , validation for email box , password box , and login btn are in the same tc
    @Test
    public void testLogin() throws Exception {
        LoginPage lp = new LoginPage(driver);
        Thread.sleep(6000);
        lp.clickBtnForLoginPage();
        Thread.sleep(6000);
        lp.setUserName(userName);
        lp.setPassword(password);
        lp.clickLoginBtn();
        Thread.sleep(6000);
        Assert.assertTrue(lp.invalidCredentials.isDisplayed(),"Error message is not displayed");
    }
}
