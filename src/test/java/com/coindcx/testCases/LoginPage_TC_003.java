package com.coindcx.testCases;

import com.coindcx.pageObjects.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPage_TC_003 extends BaseClass{
    //Validate login btn is disabled till email and password are not passed
    @Test
    public void testLogin() throws InterruptedException {
        LoginPage lp = new LoginPage(driver);
        Thread.sleep(6000);
        lp.clickBtnForLoginPage();
        Thread.sleep(5000);
        Assert.assertTrue(!(lp.disabledloginbtn.isEnabled()),"Login btn is not disabled");
    }
}
